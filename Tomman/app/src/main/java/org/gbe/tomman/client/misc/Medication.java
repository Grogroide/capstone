package org.gbe.tomman.client.misc;

import android.os.Parcel;
import android.os.Parcelable;

import org.gbe.tomman.client.retrofit.MedicationDTO;

/**
 * Created by gbe on 11/10/14.
 */
public class Medication implements Parcelable {

    long id;
    String name;
    String description;

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeString(description);
    }

    @Override
    public String toString() {
        return name;
    }

    public Medication(MedicationDTO dto) {
        this.id = dto.getId();
        this.name = dto.getName();
        this.description = dto.getDescription();
    }

    public MedicationDTO getMedicationDto() {
        MedicationDTO dto = new MedicationDTO();
        dto.setId(id);
        dto.setName(name);
        dto.setDescription(description);

        return dto;
    }

    public Medication(Parcel in) {
        this.id = in.readLong();
        this.name = in.readString();
        this.description = in.readString();
    }

    public static final Parcelable.Creator<Medication> CREATOR
            = new Parcelable.Creator<Medication>() {

        @Override
        public Medication createFromParcel(Parcel parcel) {
            return new Medication(parcel);
        }

        @Override
        public Medication[] newArray(int i) {
            return new Medication[i];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
