package org.gbe.tomman.Patient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.util.Log;
import android.view.GestureDetector;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.widget.Button;

import org.gbe.tomman.R;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class ReminderActivity extends Activity {

    private GestureDetectorCompat mDetector;

    private static final String TAG = "ReminderActivity";

    @InjectView(R.id.btn_snooze)
    Button btnSnooze;

    @InjectView(R.id.btn_goto_checkin)
    Button btnGotoCheckin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reminder);
        ButterKnife.inject(this);
        mDetector = new GestureDetectorCompat(this, new MyGestureListener());
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    @OnClick(R.id.btn_goto_checkin)
    protected void launchCheckinActivity() {
        // TODO : broken after the change to fragments
        startActivity(new Intent(ReminderActivity.this, FullCheckInFragment.class));
    }

    @OnClick(R.id.btn_snooze)
    protected void closeSelf() {
        this.finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.reminder, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";
        @Override
        public boolean onFling(MotionEvent event1, MotionEvent event2, float velocityX, float velocityY) {
            Log.d(DEBUG_TAG, "onFling: " + event1.toString()+event2.toString());
            if(velocityX < 0) {
                closeSelf();
            } else if (velocityX > 0 ) {
                launchCheckinActivity();
            }
            return true;
        }
    }
}
