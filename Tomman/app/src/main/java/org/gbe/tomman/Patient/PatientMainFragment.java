package org.gbe.tomman.Patient;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import org.gbe.tomman.LoginActivity;
import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Medication;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.Reminder;
import org.gbe.tomman.client.misc.TommanPrefs;
import org.gbe.tomman.client.retrofit.PatientDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;


public class PatientMainFragment extends Fragment {

    @InjectView(R.id.tv_user_id)
    TextView userId;

    @InjectView(R.id.btn_prescriptions)
    Button btnPrescriptions;

    @InjectView(R.id.btn_checkin)
    Button btnCheckin;

    @InjectView(R.id.btn_history)
    Button btnHistory;

    @InjectView(R.id.btn_settings)
    Button btnSettings;

    private Patient patient;

    private TommanSvcApi tomman;

    private TommanPrefs prefs;
    private Reminder remindersManager;

    private OnPatientMainScreenButtonClickedListener mActivityCallback;

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        setContentView(R.layout.fragment_patient_main);
        View view = inflater.inflate(R.layout.fragment_patient_main, container, false);
        ButterKnife.inject(this, view);
        return  view;
    }

    @Override
    public void onResume(){
        super.onResume();
        prefs = new TommanPrefs(getActivity());
        tomman = TommanSvc.getOrShowLogin(getActivity());
        if(tomman != null) {
            String s = prefs.getPreferences().getString(TommanPrefs.LOGGED_AS, null);
            if (TommanPrefs.PATIENT.equals(s)) {
                fetchPatient();
                remindersManager = new Reminder();
                remindersManager.init(getActivity());
            } else {
                launchLoginActivity();
            }
        }
    }

    private void fetchPatient() {
        CallableTask.invoke(
                new Callable<PatientDTO>() {
                    @Override
                    public PatientDTO call() throws Exception {
                        return tomman.getPatientInfo();
                    }
                },
                new TaskCallback<PatientDTO>() {
                    @Override
                    public void success(PatientDTO result) {
                        patient = new Patient(result);
                        userId.setText(patient.getFirstName() + " " + patient.getLastName());
                    }

                    @Override
                    public void error(Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), "Error while retrieving patient info", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
        );
    }

    private void launchLoginActivity() {
        Intent i = new Intent(this.getActivity(), LoginActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btn_prescriptions)
    public void launchPrescriptionsActivity() {
//        Intent i = new Intent(PatientMainFragment.this, PrescriptionsActivity.class);
//        if (patient != null) {
//            i.putParcelableArrayListExtra("prescriptions", new ArrayList<Medication>(patient.getPrescriptions()));
//            startActivity(i);
//        }
        mActivityCallback.onPrescriptionsButtonClicked(new ArrayList<Medication>((patient.getPrescriptions())));
    }

    @OnClick(R.id.btn_checkin)
    public void launchCheckInActivity() {
//        Intent i = new Intent(PatientMainFragment.this, FullCheckInFragment.class);
////        i.putExtra(LoginActivity.PATIENT_KEY, patient);
//        startActivity(i);
        mActivityCallback.onCheckInButtonClicked();
    }

    @OnClick(R.id.btn_history)
    public void launchHistoryActivity() {
        mActivityCallback.onHistoryButtonClicked(patient);
//        Intent i = new Intent(PatientMainFragment.this, HistoryActivity.class);
//        i.putExtra(LoginActivity.PATIENT_KEY, patient);
//        startActivity(i);
    }

    @OnClick(R.id.btn_settings)
    public void launchSettingsActivity() {
        mActivityCallback.onPatientSettingsButtonClicked();
//        Intent i = new Intent(PatientMainFragment.this, PatientSettingsActivity.class);
//        startActivity(i);
    }

    public interface OnPatientMainScreenButtonClickedListener {
        void onCheckInButtonClicked();
        void onHistoryButtonClicked(Patient p);
        void onPrescriptionsButtonClicked(ArrayList<Medication> prescriptions);
        void onPatientSettingsButtonClicked();
    }

    @Override
    public void onAttach(Activity hostActivity) {
        super.onAttach(hostActivity);
        try {
            mActivityCallback = (OnPatientMainScreenButtonClickedListener)hostActivity;
        }catch(ClassCastException e) {
            throw new ClassCastException(hostActivity.toString() +
            " must implement OnPatientMainScreenButtonClickedListener");
        }
    }
}
