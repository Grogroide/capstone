package org.gbe.tomman.client.retrofit;


import java.util.Collection;
import java.util.HashSet;

import com.google.common.base.Objects;


public class PatientDTO implements Person {
    private long id;
    private String firstName;
    private String lastName;
    private String userName;
    private String medicalRecordNumber;
    private long dateOfBirth;

    private Collection<MedicationDTO> prescriptions;

    private Collection<Long> doctors;



    /* (non-Javadoc)
     * @see org.gbe.capstone.tomman.webservice.retrofit.Person#getId()
     */
    @Override
    public long getId() {
        return id;
    }

    /* (non-Javadoc)
     * @see org.gbe.capstone.tomman.webservice.retrofit.Person#setId(long)
     */
    @Override
    public void setId(long id) {
        this.id = id;
    }

    /* (non-Javadoc)
     * @see org.gbe.capstone.tomman.webservice.retrofit.Person#getFirstName()
     */
    @Override
    public String getFirstName() {
        return firstName;
    }

    /* (non-Javadoc)
     * @see org.gbe.capstone.tomman.webservice.retrofit.Person#setFirstName(java.lang.String)
     */
    @Override
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /* (non-Javadoc)
     * @see org.gbe.capstone.tomman.webservice.retrofit.Person#getLastName()
     */
    @Override
    public String getLastName() {
        return lastName;
    }

    /* (non-Javadoc)
     * @see org.gbe.capstone.tomman.webservice.retrofit.Person#setLastName(java.lang.String)
     */
    @Override
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /* (non-Javadoc)
     * @see org.gbe.capstone.tomman.webservice.retrofit.Person#getUserName()
     */
    @Override
    public String getUserName() {
        return userName;
    }

    /* (non-Javadoc)
     * @see org.gbe.capstone.tomman.webservice.retrofit.Person#setUserName(java.lang.String)
     */
    @Override
    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public Collection<MedicationDTO> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(Collection<MedicationDTO> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public Collection<Long> getDoctors() {
        return doctors;
    }

    public void setDoctors(Collection<Long> doctors) {
        this.doctors = doctors;
    }

    @Override
    public int hashCode() {
        // Google Guava provides great utilities for hashing
        return Objects.hashCode(id, userName, medicalRecordNumber);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof PatientDTO) {
            PatientDTO other = (PatientDTO) obj;
            // Google Guava provides great utilities for equals too!
            return Objects.equal(id, other.id)
                    && Objects.equal(userName, other.userName)
                    && Objects.equal(medicalRecordNumber, other.medicalRecordNumber);
        } else {
            return false;
        }
    }
}
