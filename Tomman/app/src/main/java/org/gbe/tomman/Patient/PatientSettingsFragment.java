package org.gbe.tomman.Patient;

import android.app.Activity;
import android.app.Fragment;
import android.app.TimePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import org.gbe.tomman.R;
import org.gbe.tomman.client.misc.Reminder;
import org.gbe.tomman.client.misc.TommanPrefs;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class PatientSettingsFragment extends Fragment {

    private TommanPrefs prefs;

    private Calendar quietTimeStart;
    private Calendar quietTimeEnd;
    private Reminder remindersManager;

    @InjectView(R.id.chBox_enableReminders)
    CheckBox chBoxEnableReminders;

    @InjectView(R.id.tv_quiet_time_start_active)
    TextView tvQuietTimeStartActive;

    @InjectView(R.id.tv_quiet_time_end_active)
    TextView tvQuietTimeEndActive;

    @InjectView(R.id.btn_patient_settings_save)
    Button btnPatientSettingsSave;


    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_patient_settings, container, false);
        ButterKnife.inject(this, view);
        prefs = new TommanPrefs(getActivity());
        remindersManager = new Reminder();
        remindersManager.init(getActivity());
        quietTimeEnd =  Calendar.getInstance();
        quietTimeStart = Calendar.getInstance();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        read();
    }

    private void apply() {
        remindersManager.enableReminder(getActivity(), chBoxEnableReminders.isChecked());
        remindersManager.setQuietTime(getActivity(), quietTimeStart.getTime(), quietTimeEnd.getTime());
    }

    private void read() {
        quietTimeStart.setTimeInMillis(remindersManager.getQuietTimeStart(getActivity()));
        quietTimeEnd.setTimeInMillis(remindersManager.getQuietTimeEnd(getActivity()));
        chBoxEnableReminders.setChecked(remindersManager.isReminderEnabled(getActivity()));
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        tvQuietTimeStartActive.setText(sdf.format(quietTimeStart.getTime()));
        tvQuietTimeEndActive.setText(sdf.format(quietTimeEnd.getTime()));
    }

    @OnClick(R.id.tv_quiet_time_start_active)
    protected void promptForQuietTimeStart() {
        TimePickerDialog d = new TimePickerDialog(
                getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i2) {

                        quietTimeStart.set(Calendar.HOUR_OF_DAY, i);
                        quietTimeStart.set(Calendar.MINUTE, i2);
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                        tvQuietTimeStartActive.setText(sdf.format(quietTimeStart.getTime()));
                    }
                },
                quietTimeStart.get(Calendar.HOUR_OF_DAY),
                quietTimeStart.get(Calendar.MINUTE),
                true);
        d.show();
    }

    @OnClick(R.id.tv_quiet_time_end_active)
    protected void promptForQuietTimeEnd() {
        TimePickerDialog d = new TimePickerDialog(
                getActivity(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i2) {

                        quietTimeEnd.set(Calendar.HOUR_OF_DAY, i);
                        quietTimeEnd.set(Calendar.MINUTE, i2);
                        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                        tvQuietTimeEndActive.setText(sdf.format(quietTimeEnd.getTime()));
                    }
                },
                quietTimeEnd.get(Calendar.HOUR_OF_DAY),
                quietTimeEnd.get(Calendar.MINUTE),
                true);
        d.show();
    }

    @OnClick(R.id.btn_patient_settings_save)
    protected void saveSettings() {
        if(validate())
            apply();
    }

    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    private boolean validate() {
        Calendar today = Calendar.getInstance();
        quietTimeStart.set(Calendar.DAY_OF_YEAR, today.get(Calendar.DAY_OF_YEAR));
        quietTimeStart.set(Calendar.YEAR, today.get(Calendar.YEAR));
        quietTimeEnd.set(Calendar.DAY_OF_YEAR, today.get(Calendar.DAY_OF_YEAR));
        quietTimeEnd.set(Calendar.YEAR, today.get(Calendar.YEAR));
        if (quietTimeEnd.before(quietTimeStart)) {
            quietTimeEnd.add(Calendar.DATE, 1);
        }
        if (getDateDiff(quietTimeStart.getTime(), quietTimeEnd.getTime(), TimeUnit.MINUTES) > (60 * 12)) {
            Toast.makeText(getActivity(), "Quiet times cannot exceed 12 hours", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;
    }
}
