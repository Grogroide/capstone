package org.gbe.tomman.Doctor;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import android.util.Log;

import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.PatientAdapter;
import org.gbe.tomman.client.retrofit.PatientDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;


public class AlertsListFragment extends Fragment {

    private TommanSvcApi tomman;

    private static final String TAG = "AlertsListActivity";

    private OnPatientSelectedListener mCallback;

    @InjectView(R.id.lv_alerts)
    ListView lvAlerts;

    private List<Patient> patients;
    private PatientAdapter patientAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_alerts_list, container, false);
        ButterKnife.inject(this, view);
        patients = new ArrayList<Patient>();
        patientAdapter =  new PatientAdapter(getActivity(), R.layout.patient_row, patients);
        lvAlerts.setAdapter(patientAdapter);
        return view;
    }
    @Override
    public void onStart() {
        super.onStart();
        // When in two-pane layout, set the listview to highlight the selected list item
        // (We do this during onStart because at the point the listview is available.)
        if (getFragmentManager().findFragmentById(R.id.rightPanel) != null) {
            lvAlerts.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }
    @Override
    public void onResume() {
        super.onResume();
        tomman = TommanSvc.getOrShowLogin(getActivity());
        fetchAlerts();
    }

    @Override
    public void onAttach(Activity hostActivity){
        super.onAttach(hostActivity);
        try{
            mCallback = (OnPatientSelectedListener)hostActivity;
        } catch(ClassCastException e) {
            throw new ClassCastException(hostActivity.toString() +
                    "must implement OnPatientSelectedListener");
        }
    }

    private void fetchAlerts() {
        CallableTask.invoke(
                new Callable<Collection<PatientDTO>>(){
                    @Override
                    public Collection<PatientDTO> call() throws Exception {
                        Calendar c = Calendar.getInstance();
                        c.add(Calendar.DATE, -2);
                        return tomman.getAlerts(c.getTimeInMillis());
                    }
                },
                new TaskCallback<Collection<PatientDTO>>() {
                    @Override
                    public void success(Collection<PatientDTO> result) {
                        patients.clear();
                        for(PatientDTO dto : result ) {
                            patients.add(new Patient(dto));
                            patientAdapter.notifyDataSetChanged();
                        }
                    }

                    @Override
                    public void error(Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(),
                                "Error while retrieving alerts info", Toast.LENGTH_SHORT).show();
                        Log.e(TAG, "Error in tomman.getAlerts()", e);
                    }
                }
        );
    }

    @OnItemClick(R.id.lv_alerts)
    void onItemClick(int position){
        Patient p = patients.get(position);
        mCallback.onPatientSelected(p);
        lvAlerts.setItemChecked(position, true);
    }

    public interface OnPatientSelectedListener {
        void onPatientSelected(Patient p) ;
    }
}
