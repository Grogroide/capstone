/* 
**
** Copyright 2014, Jules White
**
** 
*/
package org.gbe.tomman.client;

import java.util.concurrent.Callable;

import android.os.AsyncTask;
import android.util.Log;


public class CallableTask<T> extends AsyncTask<Void,Double,T> {

    private static final String TAG = CallableTask.class.getName();

    public static <V> void invoke(Callable<V> call, TaskCallback<V> callback){
        new CallableTask<V>(call, callback).execute();
    }

    private Callable<T> callable;

    private TaskCallback<T> callback;

    private Exception error_;

    public CallableTask(Callable<T> _callable, TaskCallback<T> _callback) {
        callable = _callable;
        callback = _callback;
    }

    @Override
    protected T doInBackground(Void... ts) {
        T result = null;
        try{
            result = callable.call();
        } catch (Exception e){
            Log.e(TAG, "Error invoking callable in AsyncTask callable: "+ callable, e);
            error_ = e;
        }
        return result;
    }

    @Override
    protected void onPostExecute(T r) {
        if(error_ != null){
            callback.error(error_);
        }
        else {
            callback.success(r);
        }
    }
}
