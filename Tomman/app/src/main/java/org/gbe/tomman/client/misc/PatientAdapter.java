package org.gbe.tomman.client.misc;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.gbe.tomman.R;

import java.util.List;

/**
 * Created by gbe on 11/21/14.
 */
public class PatientAdapter extends ArrayAdapter<Patient>{

    private final List<Patient> data;
    private Context context;
    private int layoutResourceId;


    public PatientAdapter(Context context, int textViewResourceId, List<Patient> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
        this.layoutResourceId = textViewResourceId;
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row =  convertView;
        PatientHolder holder;
        if(row ==null) {
            row = LayoutInflater.from(context).inflate(layoutResourceId, parent, false);
            holder = new PatientHolder();
            holder.tvPatientLine1 = (TextView)row.findViewById(R.id.tv_patient_line1);
            holder.tvPatientLine2 = (TextView)row.findViewById(R.id.tv_patient_line2);
            row.setTag(holder);
        }
        else{
            holder = (PatientHolder)row.getTag();
        }

        Patient p = data.get(position);
        holder.tvPatientLine1.setText(p.getFirstName() + " " + p.getLastName());
        holder.tvPatientLine2.setText(p.getMedicalRecordNumber());

        return row;
    }

    static class PatientHolder{
        TextView tvPatientLine1;
        TextView tvPatientLine2;
    }
}
