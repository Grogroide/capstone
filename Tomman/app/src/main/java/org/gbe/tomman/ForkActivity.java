//package org.gbe.tomman;
//
//import android.app.Activity;
//import android.content.Intent;
//import android.os.Bundle;
//import android.view.Menu;
//import android.view.MenuItem;
//import android.widget.Button;
//
//import org.gbe.tomman.Doctor.DoctorMainFragmentedActivity;
//import org.gbe.tomman.Patient.PatientMainFragmentedActivity;
//import org.gbe.tomman.client.misc.TommanPrefs;
//
//import butterknife.ButterKnife;
//import butterknife.InjectView;
//import butterknife.OnClick;
//
//
//public class ForkActivity extends Activity {
//
//    private TommanPrefs prefs;
//
//    @InjectView(R.id.button_fork_to_patient)
//    protected Button patientButton;
//
//    @InjectView(R.id.button_fork_to_doctor)
//    protected Button doctorButton;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_fork);
//        ButterKnife.inject(this);
//        prefs = new TommanPrefs(getApplicationContext());
//        String s = prefs.getPreferences().getString(TommanPrefs.LOGGED_AS, null);
//        if (TommanPrefs.PATIENT.equals(s)){
//            launchPatientMainActivity();
//        }else{
//            if (TommanPrefs.DOCTOR.equals(s)){
//                launchDoctorMainActivity();
//            }
//        }
//    }
//
//    @OnClick(R.id.button_fork_to_doctor)
//    public void launchDoctorMainActivity(){
//        Intent doctorMainIntent = new Intent(this, DoctorMainFragmentedActivity.class);
////        doctorButton.animate().setDuration(100).scaleX(1.2f).scaleY(1.2f).start();
////        doctorButton.animate().setDuration(100).scaleX(1.0f).scaleY(1.0f).start();
//        prefs.getPreferences().edit().putString(TommanPrefs.LOGGING_AS, TommanPrefs.DOCTOR).commit();
//        startActivity(doctorMainIntent);
//    }
////    @OnTouch(R.id.button_fork_to_doctor)
////    public boolean onTouchEvent(MotionEvent event) {
////        switch (event.getAction()) {
////            case MotionEvent.ACTION_DOWN:
////                doctorButton.animate().setDuration(100).scaleX(1.2f).scaleY(1.2f).start();
////                return true;
////            case MotionEvent.ACTION_CANCEL:
////                doctorButton.animate().setDuration(100).scaleX(1.0f).scaleY(1.0f).start();
////                return true;
////            case MotionEvent.ACTION_UP:
////                doctorButton.animate().setDuration(100).scaleX(1.0f).scaleY(1.0f).start();
////                prefs.getPreferences().edit().putString(TommanPrefs.LOGGING_AS, TommanPrefs.DOCTOR).commit();
////                startActivity(new Intent(this, MainDoctorFragmentedActivity.class));
////                return true;
////            default :
////                return false;
////        }
////    }
//
//    @OnClick(R.id.button_fork_to_patient)
//    public void launchPatientMainActivity(){
//        Intent patientMainIntent = new Intent(this, PatientMainFragmentedActivity.class);
////        patientButton.animate().setDuration(100).scaleX(1.2f).scaleY(1.2f).start();
////        patientButton.animate().setDuration(100).scaleX(1.0f).scaleY(1.0f).start();
//        prefs.getPreferences().edit().putString(TommanPrefs.LOGGING_AS, TommanPrefs.PATIENT).commit();
//        startActivity(patientMainIntent);
//    }
//
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        getMenuInflater().inflate(R.menu.menu_fork, menu);
//        return true;
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }
//}
