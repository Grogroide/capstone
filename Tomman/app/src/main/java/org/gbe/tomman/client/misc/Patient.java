package org.gbe.tomman.client.misc;

import android.os.Parcel;
import android.os.Parcelable;

import org.gbe.tomman.client.retrofit.MedicationDTO;

import java.util.ArrayList;
import java.util.List;

import org.gbe.tomman.client.retrofit.PatientDTO;

/**
 * Created by gbe on 11/10/14.
 */
public class Patient implements Parcelable{

    private long id;
    private String firstName;
    private String lastName;
    private String userName;
    private String medicalRecordNumber;
    private long dateOfBirth;

    private List<Medication> prescriptions;
    private List<Long> doctors;

    public Patient(Parcel in) {
        this.id = in.readLong();
        this.firstName = in.readString();
        this.lastName = in.readString();
        this.userName = in.readString();
        this.medicalRecordNumber = in.readString();
        this.dateOfBirth = in.readLong();
        prescriptions = new ArrayList<Medication>();
        in.readTypedList(prescriptions, Medication.CREATOR);
        doctors = new ArrayList<Long>();
        in.readList(doctors, Long.class.getClassLoader());
    }

    public Patient(PatientDTO dto){
        this.id = dto.getId();
        this.firstName = dto.getFirstName();
        this.lastName = dto.getLastName();
        this.userName = dto.getUserName();
        this.medicalRecordNumber = dto.getMedicalRecordNumber();
        this.dateOfBirth = dto.getDateOfBirth();
        prescriptions = new ArrayList<Medication>();
        for(MedicationDTO mdto : dto.getPrescriptions()){
            prescriptions.add(new Medication(mdto));
        }
        doctors = new ArrayList<Long>(dto.getDoctors());
    }

    public PatientDTO getPatientDTO(){
        PatientDTO p = new PatientDTO();
        p.setId(id);
        p.setFirstName(firstName);
        p.setLastName(lastName);
        p.setUserName(userName);
        p.setMedicalRecordNumber(medicalRecordNumber);
        p.setDateOfBirth(dateOfBirth);
        p.setDoctors(doctors);
        p.setPrescriptions(new ArrayList<MedicationDTO>());
        for(Medication m : prescriptions){
            p.getPrescriptions().add(m.getMedicationDto());
        }
        return p;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
        parcel.writeString(userName);
        parcel.writeString(medicalRecordNumber);
        parcel.writeLong(dateOfBirth);
        parcel.writeTypedList(prescriptions);
        parcel.writeList(doctors);
    }

    public static final Parcelable.Creator<Patient> CREATOR
            = new Parcelable.Creator<Patient>() {

        @Override
        public Patient createFromParcel(Parcel parcel) {
            return new Patient(parcel);
        }

        @Override
        public Patient[] newArray(int i) {
            return new Patient[i];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getMedicalRecordNumber() {
        return medicalRecordNumber;
    }

    public void setMedicalRecordNumber(String medicalRecordNumber) {
        this.medicalRecordNumber = medicalRecordNumber;
    }

    public long getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(long dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public List<Medication> getPrescriptions() {
        return prescriptions;
    }

    public void setPrescriptions(List<Medication> prescriptions) {
        this.prescriptions = prescriptions;
    }

    public List<Long> getDoctors() {
        return doctors;
    }

    public void setDoctors(List<Long> doctors) {
        this.doctors = doctors;
    }
}
