package org.gbe.tomman.Patient;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.google.common.collect.Iterables;

import org.gbe.tomman.LoginActivity;
import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.Report;
import org.gbe.tomman.client.misc.ReportAdapter;
import org.gbe.tomman.client.retrofit.CheckInDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;
import org.gbe.tomman.client.retrofit.TookMedicationDTO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;


public class HistoryFragment extends Fragment {

    Patient patient;

    @InjectView(R.id.lv_history)
    ListView lv_history;

    List<Report> reports;

    ArrayAdapter<Report> reportAdapter;
    TommanSvcApi tomman;

    public static HistoryFragment newInstance(Patient p) {
        // History fragment is the only class that knows how to pass argument to itself, therefore
        // keep the code local to HistoryFragment.

        // I've seen this for many of your fragment, it is very important to respect encapsulation
        // and to not leak implementation details across the code base (this will introduce bugs
        // fairly quickly when working with a team).
        HistoryFragment f = new HistoryFragment();
        Bundle b = new Bundle();
        b.putParcelable(LoginActivity.PATIENT_KEY, p);
        f.setArguments(b);

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_history, container, false);
        ButterKnife.inject(this, view);
        reports = new ArrayList<Report>();
        tomman = TommanSvc.getOrShowLogin(getActivity());
        getPatientFromArguments();
        reportAdapter = new ReportAdapter(getActivity(), R.layout.report_list_ci_row, reports);
        lv_history.setAdapter(reportAdapter);
        fetchHistory();
        return view;
    }

    private void fetchHistory() {
        CallableTask.invoke(
                new GetHistoryCallable(),
                new OnReceivedHistoryCallback()
        );
    }

    private void fetchMedicationHistory() {
        CallableTask.invoke(
                new GetMedicationHistoryCallable(),
                new OnReceivedMedicationHistoryCallback()
        );
    }

    private void getPatientFromArguments() {
        Bundle b = getArguments();
        if (b != null) {
            patient = b.getParcelable(LoginActivity.PATIENT_KEY);
        }

        if (patient == null) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }
    }

    private class GetHistoryCallable implements Callable<Collection<CheckInDTO>> {
        @Override
        public Collection<CheckInDTO> call() throws Exception {
            return tomman.getHistory();
        }
    }

    private class OnReceivedHistoryCallback implements TaskCallback<Collection<CheckInDTO>>{
        @Override
        public void success(Collection<CheckInDTO> result) {
            reports.clear();
            for(CheckInDTO dto : result){
                reports.add(new Report(dto));
            }
            try {
//                Collections.sort(reports, new Report.ReverseComparator());
                reportAdapter.notifyDataSetChanged();
            }catch(Exception ex){
                ex.printStackTrace();
            }
            fetchMedicationHistory();
        }

        @Override
        public void error(Exception e) {
            Toast.makeText(getActivity(), "Error : "+ e.getMessage() , Toast.LENGTH_LONG).show();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }
    }

    private class GetMedicationHistoryCallable implements Callable<Collection<TookMedicationDTO>> {
        @Override
        public Collection<TookMedicationDTO> call() throws Exception {
            return tomman.getMedicationHistory();
        }
    }

    private class OnReceivedMedicationHistoryCallback implements TaskCallback<Collection<TookMedicationDTO>> {

        @Override
        public void success(Collection<TookMedicationDTO> result) {
            for(TookMedicationDTO dto : result) {
                reports.add(new Report(dto));
            }
//            Collections.sort(reports, new Report.ReverseComparator());
            reportAdapter.notifyDataSetChanged();
            createDayBreakSeparators();
        }

        @Override
        public void error(Exception e) {
            Toast.makeText(getActivity(), "Error : "+ e.getMessage() , Toast.LENGTH_LONG).show();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }
    }

    private void createDayBreakSeparators() {
        Report oldestReport = Iterables.getLast(reports, null);
        if(oldestReport != null) {
            Calendar now = GregorianCalendar.getInstance();
            Calendar current = GregorianCalendar.getInstance();
            current.setTimeInMillis(oldestReport.getTimeStamp());
            current.set(Calendar.HOUR_OF_DAY, 23);
            current.set(Calendar.MINUTE, 59);
            current.set(Calendar.SECOND, 59);
            current.set(Calendar.MILLISECOND, 999);
            reports.add(new Report(current.getTimeInMillis()));
            while (current.before(now)) {
                current.add(Calendar.DATE, 1);
                reports.add(new Report(current.getTimeInMillis()));
            }
            Collections.sort(reports, new Report.ReverseComparator());
            reportAdapter.notifyDataSetChanged();
        }
    }
}
