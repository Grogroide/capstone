package org.gbe.tomman.Doctor;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import org.gbe.tomman.LoginActivity;
import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Medication;
import org.gbe.tomman.client.misc.MedicationAdapter;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.TommanPrefs;
import org.gbe.tomman.client.retrofit.MedicationDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import butterknife.OnItemLongClick;


/**
 * List of a given patient's prescribed medication, allows to add or remove a prescribed medication
 * to this patient. Accessed via the PatientReport, on the Doctor side.
 */
public class PrescriptionFragment extends Fragment {

    private TommanSvcApi tomman;
    private TommanPrefs prefs;
    private Patient patient;

    private List<MedicationDTO> patientPrescriptions;
    private List<Medication> availableMedications;

    private MedicationAdapter prescriptionListAdapter;

    private ArrayAdapter<Medication> medicationSpinnerAdapter;

    @InjectView(R.id.lv_prescriptionlist)
    ListView lvPrescriptionList;

    @InjectView(R.id.spin_prescribe)
    Spinner spinPrescribe;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_prescription, container, false);
        ButterKnife.inject(this, view);
        patientPrescriptions = new ArrayList<MedicationDTO>();
        availableMedications = new ArrayList<Medication>();
        prescriptionListAdapter = new MedicationAdapter(getActivity(), R.layout.listview_medication_row, patientPrescriptions);
        medicationSpinnerAdapter = new ArrayAdapter<Medication>(getActivity(), android.R.layout.simple_spinner_dropdown_item, availableMedications);
        lvPrescriptionList.setAdapter(prescriptionListAdapter);
        spinPrescribe.setAdapter(medicationSpinnerAdapter);
        getPatientFromArgs();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        tomman = TommanSvc.getOrShowLogin(getActivity());
        fetchPatientPrescriptions();
        fetchAvailableMedications();
    }

    @OnClick(R.id.btn_prescribe)
    protected void prescribe(){
        CallableTask.invoke(
                new PostPrescribeCallable(),
                new OnPostedPrescribeCallback()
        );
    }

    @OnItemLongClick(R.id.lv_prescriptionlist)
    protected boolean unprescribe(int position) {
        MedicationDTO dto = (MedicationDTO) lvPrescriptionList.getItemAtPosition(position);
        CallableTask.invoke(
                new PostUnprescribeCallable(patient, dto),
                new OnPostedUnprescribeCallback()
        );
        return true;
    }

    private void fetchPatientPrescriptions() {
        CallableTask.invoke(
            new GetPatientPrescriptionsCallable(),
            new OnPatientPrescriptionsReceivedCallback()
        );
    }

    private void fetchAvailableMedications() {
        CallableTask.invoke(
            new GetMedicationsCallable(),
            new OnMedicationsReceivedCallback()
        );
    }

    private class GetPatientPrescriptionsCallable implements Callable<Collection<MedicationDTO>> {

        @Override
        public Collection<MedicationDTO> call() throws Exception {
            return tomman.getPatientInfo(patient.getId()).getPrescriptions();
        }
    }

    private class OnPatientPrescriptionsReceivedCallback implements TaskCallback<Collection<MedicationDTO>> {

        @Override
        public void success(Collection<MedicationDTO> result) {
            patientPrescriptions.clear();
            patientPrescriptions.addAll(result);
            prescriptionListAdapter.notifyDataSetChanged();
        }

        @Override
        public void error(Exception e) {
            Toast.makeText(getActivity(), "Error : failed to retrieve patient info", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private class GetMedicationsCallable implements Callable<Collection<MedicationDTO>> {

        @Override
        public Collection<MedicationDTO> call() throws Exception {
            return tomman.getMedications();
        }
    }

    private class OnMedicationsReceivedCallback implements TaskCallback<Collection<MedicationDTO>> {

        @Override
        public void success(Collection<MedicationDTO> result) {
            availableMedications.clear();
            for(MedicationDTO dto : result){
                availableMedications.add(new Medication(dto));

            }
            medicationSpinnerAdapter.notifyDataSetChanged();
        }

        @Override
        public void error(Exception e) {
            Toast.makeText(getActivity(), "Error : failed to retrieve medication list", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private class PostPrescribeCallable implements Callable<Boolean> {

        @Override
        public Boolean call() throws Exception {
            return tomman.prescribe(patient.getId(), ((Medication) spinPrescribe.getSelectedItem()).getId());
        }
    }
    private class OnPostedPrescribeCallback implements TaskCallback<Boolean> {

        @Override
        public void success(Boolean result) {
            if(!result) {
                error(new Exception("Invalid argument or authorization issue"));
            }else {
                fetchPatientPrescriptions();
            }
        }

        @Override
        public void error(Exception e) {
            Toast.makeText(getActivity(), "Error : failed to communicate with server", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }

    private class PostUnprescribeCallable implements Callable<Boolean>{

        Patient patient;
        MedicationDTO medication;
        public PostUnprescribeCallable(Patient p, MedicationDTO med){
            patient = p;
            medication = med;
        }
        @Override
        public Boolean call() throws Exception {
            return tomman.unprescribe(patient.getId(), medication.getId());
        }
    }
    private class OnPostedUnprescribeCallback implements TaskCallback<Boolean> {

        @Override
        public void success(Boolean result) {
            if(!result) {
                error(new Exception("Invalid argument or authorization issue"));
            }else {
                fetchPatientPrescriptions();
            }
        }

        @Override
        public void error(Exception e) {
            Toast.makeText(getActivity(), "Error : failed to communicate with server", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }
    }


    private void getPatientFromArgs() {
        Bundle b = getArguments();
        if (b.isEmpty()) {
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }else{
            patient = b.getParcelable(PatientReportFragment.PATIENT_KEY);
        }
    }
}
