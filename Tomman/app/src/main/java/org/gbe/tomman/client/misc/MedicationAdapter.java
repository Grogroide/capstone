package org.gbe.tomman.client.misc;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.gbe.tomman.R;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.gbe.tomman.client.retrofit.MedicationDTO;

/**
 * Created by gbe on 11/13/14.
 */
public class MedicationAdapter extends ArrayAdapter<MedicationDTO> {

    Context context;
    private int layoutResourceId;
//    MedicationDTO data[] = null;
    private final List<MedicationDTO> data;

    public MedicationAdapter(Context context, int layoutResourceId, MedicationDTO[] objects) {
        super(context, layoutResourceId, objects);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = new ArrayList<MedicationDTO>(Arrays.asList(objects));
    }

    public MedicationAdapter(Context context, int layoutResourceId, List<MedicationDTO> objects) {
        super(context, layoutResourceId, objects);
        this.context = context;
        this.layoutResourceId = layoutResourceId;
        this.data = objects;
                //objects.toArray(new MedicationDTO[objects.size()]);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        MedicationHolder holder;
        if (row==null) {
            LayoutInflater inflater = ((Activity) context).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new MedicationHolder();
            holder.tv_name = (TextView)row.findViewById(R.id.medication_name);
            holder.tv_description = (TextView)row.findViewById(R.id.medication_description);
            row.setTag(holder);
        }
        else {
            holder = (MedicationHolder)row.getTag();
        }

        MedicationDTO med = data.get(position);
        holder.tv_name.setText(med.getName());
        holder.tv_description.setText(med.getDescription());

        return row;
    }

    static class MedicationHolder {
        TextView tv_name;
        TextView tv_description;
    }

}
