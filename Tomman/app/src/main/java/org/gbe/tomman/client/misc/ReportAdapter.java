package org.gbe.tomman.client.misc;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import org.gbe.tomman.R;
import org.gbe.tomman.client.retrofit.CheckInDTO;
import org.gbe.tomman.client.retrofit.EatingDifficulty;
import org.gbe.tomman.client.retrofit.PainLevel;

import java.util.List;

/**
 * Created by gbe on 11/16/14.
 */
public class ReportAdapter extends ArrayAdapter<Report> {

    private static final String TAG = "ReportAdapter";

    Context context;
    private int layoutResourceId;
    final private List<Report> data;


    public ReportAdapter(Context context, int resource, List<Report> objects) {
        super(context, resource, objects);
        this.context = context;
        //this.layoutResourceId = resource;
        this.data = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        Report r = data.get(position);

        if (row != null) {  // Try to recycle
            ReportHolder holder = (ReportHolder) row.getTag();
            if (r.getType() == holder.type) { // recycle
                switch (r.getType()) {
                    case Report.TOOKMEDICATION:
                        ReportTookMedicationHolder tm_holder = (ReportTookMedicationHolder)holder;
                        tm_holder.tvTime.setText(r.getTimeString());
                        tm_holder.tvReport.setText(context.getString(R.string.report_took_string)
                                + " " + r.get_tm().getMedication().getName());
                        row.setTag(tm_holder);
                        Log.d(TAG, "Recycled TookMedication View");
                        break;
                    case Report.CHECKIN:
                        ReportCheckInHolder ci_holder = (ReportCheckInHolder)holder;
                        ci_holder.llCiRow.setBackgroundColor(colorCode(r));
                        ci_holder.tvTime1.setText(r.getTimeString());
                        ci_holder.tvTime2.setText("");
                        ci_holder.tvReportLine1.setText(context.getString(R.string.report_pain_string)
                                + " " + r.getCi().getPainLevel().toString());
                        ci_holder.tvReportLine2.setText(
                                context.getString(R.string.report_eat_difficulty_string) + " "
                                        + r.getCi().getEatingDifficulty().toString());
                        row.setTag(ci_holder);
                        Log.d(TAG, "Recycled CheckIn View");
                        break;
                    case Report.DAYBREAK:
                        ReportDayBreakHolder db_holder = (ReportDayBreakHolder)holder;
                        db_holder.tvDaybreak.setText(r.getDayString());
                        row.setTag(db_holder);
                        Log.d(TAG, "Recycled DayBreak View");
                        break;
                }
                return row;
            }
            Log.w(TAG, "Received the wrong type of View to recycle, oldView type = " + holder.type + ", newView type = " + r.getType());
        }
        // Could not recycle, inflate a new View
        LayoutInflater inflater = LayoutInflater.from(context);
        switch (r.getType()) {
            case (Report.DAYBREAK):
                ReportDayBreakHolder db_holder = new ReportDayBreakHolder();
                db_holder.type = Report.DAYBREAK;
                row = inflater.inflate(R.layout.report_list_daybreak, parent, false);
                db_holder.tvDaybreak = (TextView) row.findViewById(R.id.tv_daybreak);
                db_holder.tvDaybreak.setText(r.getDayString());
                row.setTag(db_holder);
                break;
            case Report.TOOKMEDICATION:
                ReportTookMedicationHolder tm_holder = new ReportTookMedicationHolder();
                tm_holder.type = Report.TOOKMEDICATION;
                row = inflater.inflate(R.layout.report_list_tm_row, parent, false);
                tm_holder.tvTime = (TextView)row.findViewById(R.id.tv_time);
                tm_holder.tvReport = (TextView)row.findViewById(R.id.tv_report);
                tm_holder.tvTime.setText(r.getTimeString());
                tm_holder.tvReport.setText(context.getString(R.string.report_took_string)
                        + " " + r.get_tm().getMedication().getName());
                row.setTag(tm_holder);
                break;
            case Report.CHECKIN:
                ReportCheckInHolder ci_holder = new ReportCheckInHolder();
                ci_holder.type = Report.CHECKIN;
                row = inflater.inflate(R.layout.report_list_ci_row, parent, false);
                ci_holder.llCiRow = (LinearLayout)row.findViewById(R.id.ll_ci_row);
                ci_holder.tvTime1 = (TextView)row.findViewById(R.id.tv_time_1);
                ci_holder.tvTime2 = (TextView)row.findViewById(R.id.tv_time_2);
                ci_holder.tvReportLine1 = (TextView)row.findViewById(R.id.tv_report_line1);
                ci_holder.tvReportLine2 = (TextView)row.findViewById(R.id.tv_report_line2);

                ci_holder.llCiRow.setBackgroundColor(colorCode(r));
                ci_holder.tvTime1.setText(r.getTimeString());
                ci_holder.tvTime2.setText("");
                ci_holder.tvReportLine1.setText(context.getString(R.string.report_pain_string)
                        + " " + r.getCi().getPainLevel().toString());
                ci_holder.tvReportLine2.setText(
                        context.getString(R.string.report_eat_difficulty_string) + " "
                                + r.getCi().getEatingDifficulty().toString());
                row.setTag(ci_holder);
                break;
            default:
                break;
        }

        return row;
    }

    private int colorCode(Report r) {
        if (r.getType() != Report.CHECKIN) return 0;
        CheckInDTO ci =  r.getCi();
        if ((ci.getEatingDifficulty() == EatingDifficulty.NO) && (ci.getPainLevel() == PainLevel.WELL_CONTROLLED))
            return getContext().getResources().getColor(R.color.report_light);
        if ((ci.getEatingDifficulty() == EatingDifficulty.I_CANT_EAT) || (ci.getPainLevel() == PainLevel.SEVERE))
            return getContext().getResources().getColor(R.color.report_dark);
        return getContext().getResources().getColor(R.color.report_medium);
    }

    @Override
    public int getViewTypeCount(){
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        return data.get(position).getType();
    }

    static class ReportHolder {
        int type;
    }

    static class ReportCheckInHolder extends ReportHolder {
        LinearLayout llCiRow;
        TextView tvTime1;
        TextView tvTime2;
        TextView tvReportLine1;
        TextView tvReportLine2;
    }

    static class ReportTookMedicationHolder extends ReportHolder{
        TextView tvTime;
        TextView tvReport;
    }

    static class ReportDayBreakHolder extends ReportHolder{
         TextView tvDaybreak;
    }
}
