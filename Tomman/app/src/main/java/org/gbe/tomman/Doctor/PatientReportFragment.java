package org.gbe.tomman.Doctor;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.google.common.collect.Iterables;

import org.gbe.tomman.LoginActivity;
import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.Report;
import org.gbe.tomman.client.misc.ReportAdapter;
import org.gbe.tomman.client.retrofit.CheckInDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;
import org.gbe.tomman.client.retrofit.TookMedicationDTO;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class PatientReportFragment extends Fragment {

    public static final String PATIENT_KEY = "patient";
    @InjectView(R.id.lv_patientreport)
    ListView lv_patientreport;

    TommanSvcApi tomman;
    Patient patient;
    List<Report> reports;
    ReportAdapter reportAdapter;

    OnPrescriptionButtonClickedListener mPrescriptionButtonClickedCallback;

    public interface OnPrescriptionButtonClickedListener {
        void onPrescriptionButtonClicked(Patient p);
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_patient_report, container, false);
        ButterKnife.inject(this, view);
        tomman = TommanSvc.getOrShowLogin(getActivity());
        reports = new ArrayList<Report>();
        reportAdapter = new ReportAdapter(getActivity(), 0, reports);
        lv_patientreport.setAdapter(reportAdapter);
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        Bundle args = getArguments();
        if(args != null) {
            setPatient((Patient)args.getParcelable(PATIENT_KEY));
        }
//      else{
//            // Nothing for now, may change in the future if we start using an index instead
//        }
        fetchPatientHistory();
    }

    @OnClick(R.id.btn_prescriptions)
    protected void launchPrescriptionActivity(){
        mPrescriptionButtonClickedCallback.onPrescriptionButtonClicked(patient);
    }

    @Override
    public void onAttach(Activity hostActivity){
        super.onAttach(hostActivity);
        try {
            mPrescriptionButtonClickedCallback = (OnPrescriptionButtonClickedListener)hostActivity;
        } catch (ClassCastException e) {
            throw new ClassCastException(hostActivity.toString() +
                    "must implement OnPrescriptionButtonClickedListener");
        }
    }


    public void setPatient(Patient p) {
        patient = p;
    }

    private void fetchPatientHistory() {
        CallableTask.invoke(
                new GetPatientHistoryCallable(),
                new OnReceivedPatientHistoryCallback()
        );
    }

    private void fetchPatientMedicationHistory() {
        CallableTask.invoke(
                new GetPatientMedicationHistoryCallable(),
                new OnReceivedPatientMedicationHistoryCallback()
        );
    }
    private class GetPatientHistoryCallable implements Callable<Collection<CheckInDTO>> {

        @Override
        public Collection<CheckInDTO> call() throws Exception {
            return tomman.getHistoryOfPatient(patient.getId());
        }
    }

    private class OnReceivedPatientHistoryCallback implements TaskCallback<Collection<CheckInDTO>> {
        @Override
        public void success(Collection<CheckInDTO> result) {
            reports.clear();
            for(CheckInDTO dto : result){
                reports.add(new Report(dto));
            }
            try {
//                Collections.sort(reports, new Report.ReverseComparator());
                reportAdapter.notifyDataSetChanged();
            }catch(Exception ex){
                ex.printStackTrace();
            }
            fetchPatientMedicationHistory();
        }

        @Override
        public void error(Exception e) {
            Toast.makeText(getActivity(), "Error : " + e.getMessage(), Toast.LENGTH_LONG).show();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }
    }

    private class GetPatientMedicationHistoryCallable implements Callable<Collection<TookMedicationDTO>> {
        @Override
        public Collection<TookMedicationDTO> call() throws Exception {
            return tomman.getMedicationHistoryOfPatient(patient.getId());
        }
    }

    private class OnReceivedPatientMedicationHistoryCallback implements TaskCallback<Collection<TookMedicationDTO>> {

        @Override
        public void success(Collection<TookMedicationDTO> result) {
            for(TookMedicationDTO dto : result) {
                reports.add(new Report(dto));
            }
//            Collections.sort(reports, new Report.ReverseComparator());
            reportAdapter.notifyDataSetChanged();
            createDayBreakSeparators();
        }

        @Override
        public void error(Exception e) {
            Toast.makeText(getActivity(), "Error : "+ e.getMessage() , Toast.LENGTH_LONG).show();
            startActivity(new Intent(getActivity(), LoginActivity.class));
        }
    }

    private void createDayBreakSeparators() {
        Report oldestReport = Iterables.getLast(reports, null);
        if(oldestReport != null) {
            Calendar now = GregorianCalendar.getInstance();
            Calendar current = GregorianCalendar.getInstance();
            current.setTimeInMillis(oldestReport.getTimeStamp());
            current.set(Calendar.HOUR_OF_DAY, 23);
            current.set(Calendar.MINUTE, 59);
            current.set(Calendar.SECOND, 59);
            current.set(Calendar.MILLISECOND, 999);
            reports.add(new Report(current.getTimeInMillis()));
            while (current.before(now)) {
                current.add(Calendar.DATE, 1);
                reports.add(new Report(current.getTimeInMillis()));
            }
            Collections.sort(reports, new Report.ReverseComparator());
            reportAdapter.notifyDataSetChanged();
        }
    }

}
