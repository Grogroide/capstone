package org.gbe.tomman.Patient;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;

import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Medication;
import org.gbe.tomman.client.misc.MedicationAdapter;
import org.gbe.tomman.client.retrofit.MedicationDTO;
import org.gbe.tomman.client.retrofit.PatientDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;

/**
 * Activity for the patient to review his prescribed medications, available from Patient main activity
 */
public class PrescriptionsFragment extends Fragment {

    public static final String KEY_PRESCRIPTIONS = "prescriptions";

    @InjectView(R.id.prescriptionsListView)
    protected ListView lvPrescriptions;

    private ArrayList<MedicationDTO> medList;
    private ArrayAdapter<MedicationDTO> medAdapter;
    private TommanSvcApi tomman;

    public static PrescriptionsFragment newInstance(ArrayList<Medication> prescriptions) {
        PrescriptionsFragment f = new PrescriptionsFragment();
        Bundle b = new Bundle();
        b.putParcelableArrayList(PrescriptionsFragment.KEY_PRESCRIPTIONS, prescriptions);
        f.setArguments(b);

        return f;
    }

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_prescriptions, container, false);
        ButterKnife.inject(this, view);
        medList = new ArrayList<MedicationDTO>();
        tomman = TommanSvc.getOrShowLogin(getActivity());
        medAdapter = new MedicationAdapter(getActivity(), R.layout.listview_medication_row, medList);
        lvPrescriptions.setAdapter(medAdapter);
        retrievePatientPrescriptionsList();
        return view;
    }

    private void getPrescriptionsFromArgs() {
        Bundle b = getArguments();
        if (b != null) {
            ArrayList<Medication> parcelList = b.getParcelableArrayList(KEY_PRESCRIPTIONS);

            for (Medication pm : parcelList) {
                medList.add(pm.getMedicationDto());
            }
        }
    }
    private void retrievePatientPrescriptionsList() {
        CallableTask.invoke(
                new Callable<PatientDTO>() {
                    @Override
                    public PatientDTO call() throws Exception {
                        return tomman.getPatientInfo();
                    }
                },
                new TaskCallback<PatientDTO>() {
                    @Override
                    public void success(PatientDTO result) {
                        medList.clear();
                        medList.addAll(result.getPrescriptions());
                        medAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void error(Exception e) {
                        Toast.makeText(getActivity(), "Error retrieving patient info", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
        );
    }
}
