package org.gbe.tomman.Doctor;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;

import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.PatientAdapter;
import org.gbe.tomman.client.retrofit.PatientDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;


public class PatientsListFragment extends Fragment {

    @InjectView(R.id.lv_patientList)
    protected ListView lvPatientList;

    private List<Patient> patients;

    private PatientAdapter patientAdapter;

    private TommanSvcApi tomman;
    private static final String TAG = "PatientsListActivity";

    private OnPatientSelectedListener mPatientSelectedCallback;


    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState     ) {
        View view = inflater.inflate(R.layout.fragment_patients_list, container, false);
        ButterKnife.inject(this, view);
        tomman  = TommanSvc.getOrShowLogin(getActivity());
        patients =  new ArrayList<Patient>();
        patientAdapter = new PatientAdapter(getActivity(), R.layout.patient_row, patients);
        lvPatientList.setAdapter(patientAdapter);
        fetchPatients();
        return view;
    }

    @Override
    public void onAttach(Activity hostActivity) {
        super.onAttach(hostActivity);
        try {
            mPatientSelectedCallback = (OnPatientSelectedListener)hostActivity;
        } catch (ClassCastException e){
            throw new ClassCastException(hostActivity.toString()
                    + "must implement OnPatientSelectedListener");
        }

    }

    @OnItemClick(R.id.lv_patientList)
    void onItemClick(int position){
        Patient p = patients.get(position);
        mPatientSelectedCallback.onPatientSelected(p);
        lvPatientList.setItemChecked(position, true);
    }

    private void fetchPatients() {
        CallableTask.invoke(
                new GetPatientsCallable(),
                new OnPatientReceivedCallback()
        );
    }

    private class GetPatientsCallable implements Callable<Collection<PatientDTO>>{
        @Override
        public Collection<PatientDTO> call() throws Exception {
            return tomman.getPatientList();
        }
    }

    private class OnPatientReceivedCallback implements TaskCallback<Collection<PatientDTO>>{

        @Override
        public void success(Collection<PatientDTO> result) {
            patients.clear();
            for(PatientDTO dto : result){
                patients.add(new Patient(dto));
            }
            patientAdapter.notifyDataSetChanged();
        }

        @Override
        public void error(Exception e) {
            Log.e(TAG, "Error while retrieving Patients : " + e.getMessage());
            e.printStackTrace();
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        // When in two-pane layout, set the listview to highlight the selected list item
        // (We do this during onStart because at this point the listview is available.)
        if (getFragmentManager().findFragmentById(R.id.leftPanel) != null) {
            lvPatientList.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    public interface OnPatientSelectedListener {
        void onPatientSelected(Patient p) ;
    }
}
