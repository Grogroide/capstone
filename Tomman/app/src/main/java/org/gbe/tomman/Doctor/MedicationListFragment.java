package org.gbe.tomman.Doctor;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;

import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.MedicationAdapter;
import org.gbe.tomman.client.retrofit.MedicationDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;


public class MedicationListFragment extends Fragment {

    private static final String TAG = "MedicationListActivity";

    @InjectView(R.id.lv_medications)
    protected ListView lvMedications;

    private List<MedicationDTO> medications;
    private ArrayAdapter<MedicationDTO> medicationAdapter;
    private TommanSvcApi tomman;


    @Override
    public View onCreateView(LayoutInflater inflater,
                                ViewGroup container,
                                Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_medication_list, container, false);
        ButterKnife.inject(this, view);
        tomman = TommanSvc.getOrShowLogin(getActivity());
        medications = new ArrayList<MedicationDTO>();
        medicationAdapter = new MedicationAdapter(getActivity(), R.layout.listview_medication_row, medications);
        lvMedications.setAdapter(medicationAdapter);
        fetchMedicationList();
        return  view;
    }

    private void fetchMedicationList() {
        CallableTask.invoke(
            new GetMedicationsCallable(),
            new OnReceivedMedicationsCallback()
        );
    }

    private class GetMedicationsCallable implements Callable<Collection<MedicationDTO>>{

        @Override
        public Collection<MedicationDTO> call() throws Exception {
            return tomman.getMedications();
        }
    }

    private class OnReceivedMedicationsCallback implements TaskCallback<Collection<MedicationDTO>> {

        @Override
        public void success(Collection<MedicationDTO> result) {
            medications.clear();
            medications.addAll(result);
            medicationAdapter.notifyDataSetChanged();
        }

        @Override
        public void error(Exception e) {
            Log.e(TAG, "Error while retrieving Medications : " + e.getMessage());
            e.printStackTrace();
        }
    }
}
