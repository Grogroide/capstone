package org.gbe.tomman.client.misc;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.widget.Toast;

import org.gbe.tomman.Patient.ReminderActivity;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Created by gbe on 11/24/14.
 */
public class Reminder extends BroadcastReceiver{
    public static final int TOMMAN_UNIQUE_ALARM_ID = 23121982;

    public static final String QUIET_TIME_START = "QUIETTIMESTART";
    public static final String QUIET_TIME_END = "QUIETTIMEEND";
    public static final String REMINDERS_ENABLED = "REMINDERS_ENABLED";
    public static final String NEXT_ALARM = "NEXT_ALARM";

    private TommanPrefs prefs;

    public void init(Context c) {
        prefs = new TommanPrefs(c);
        if(prefs.getPreferences().contains("REMINDERS_ENABLED")){
            return;
        }
        // else, we need to init the Reminder
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            Date start = sdf.parse("22:00");
            Date end = sdf.parse("9:00");
            prefs.getPreferences().edit()
                    .putBoolean("REMINDERS_ENABLED", true)
                    .putLong(QUIET_TIME_START, start.getTime())
                    .putLong(QUIET_TIME_END, end.getTime())
                    .apply();
        } catch(ParseException ex){
            Toast.makeText(c, "Error: failed to initialize Reminders :\n\t" + ex.getLocalizedMessage(), Toast.LENGTH_LONG).show();
            ex.printStackTrace();
        }
        invalidate(c);
    }

    private PendingIntent buildIntent(Context c) {
        Intent i = new Intent(c, Reminder.class);
        return PendingIntent.getBroadcast(c, TOMMAN_UNIQUE_ALARM_ID, i , PendingIntent.FLAG_ONE_SHOT);
    }

    public void invalidate(Context c) {  // to be called every time a check-in is submitted
        if (!prefs.getPreferences().contains(REMINDERS_ENABLED)) {
            init(c);
        }
        AlarmManager am = (AlarmManager)c.getSystemService(Context.ALARM_SERVICE);
        if(!isReminderEnabled(c)) { // try to cancel
            am.cancel(buildIntent(c));
            Toast.makeText(c, "Reminder has been disabled", Toast.LENGTH_LONG).show();
        } else { // set a new alarm 4 hours from now
            Calendar time = GregorianCalendar.getInstance();
            am.cancel(buildIntent(c));
            time.add(Calendar.HOUR, 4);
            if (isBetweenStartAndEnd(c, time)) {
                time.setTimeInMillis(getNextQuietTimeEnd(c));
            }
            prefs.getPreferences().edit().putLong(NEXT_ALARM, time.getTimeInMillis()).apply();
            am.set(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), buildIntent(c));
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            Toast.makeText(c, "Next reminder set for : " + sdf.format(time.getTime()), Toast.LENGTH_LONG).show();
        }
    }

    private long getNextQuietTimeEnd(Context c) {
        //Date now = new Date();
        Calendar cal_end_quiet = Calendar.getInstance();
        cal_end_quiet.setTimeInMillis(getQuietTimeEnd(c));
        Calendar cal_now = Calendar.getInstance();
        cal_end_quiet.set(Calendar.YEAR, cal_now.get(Calendar.YEAR));
        cal_end_quiet.set(Calendar.DAY_OF_YEAR, cal_now.get(Calendar.DAY_OF_YEAR));
        if (cal_end_quiet.before(cal_now)) {
            cal_end_quiet.add(Calendar.DATE, 1);
        }
        return cal_end_quiet.getTimeInMillis();
    }

    public void snooze(Context c) {  // set an alarm 5 minutes from now
        AlarmManager am = (AlarmManager)c.getSystemService(Context.ALARM_SERVICE);
        am.cancel(buildIntent(c));
        Calendar time = GregorianCalendar.getInstance();
        time.add(Calendar.MINUTE, 5);
        prefs.getPreferences().edit().putLong(NEXT_ALARM, time.getTimeInMillis()).apply();
        PendingIntent pi = buildIntent(c);
        am.set(AlarmManager.RTC_WAKEUP, time.getTimeInMillis(), pi);
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Toast.makeText(c, "Reminder will repeat itself at : " + sdf.format(time.getTime()), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onReceive(Context c, Intent intent) {
        // play a ding ding
        try
        {
            prefs = new TommanPrefs(c);
            Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            Ringtone r = RingtoneManager.getRingtone(c, notification);
            r.play();
        } catch (Exception e){
            Toast.makeText(c, "Error while playing ringtone", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }

        snooze(c);

        // launch ReminderActivity
        Intent i = new Intent(c, ReminderActivity.class);
        i.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        c.startActivity(i);
    }

    public long getQuietTimeEnd(Context c) {
        if(prefs == null) {
            prefs = new TommanPrefs(c);
        }
        return prefs.getPreferences().getLong(QUIET_TIME_END, -1);
    }
    public long getQuietTimeStart(Context c) {
        if(prefs == null) {
            prefs = new TommanPrefs(c);
        }
        return prefs.getPreferences().getLong(QUIET_TIME_START, -1);
    }
    public boolean isReminderEnabled(Context c) {
        if(prefs == null) {
            prefs = new TommanPrefs(c);
        }
        return prefs.getPreferences().getBoolean(REMINDERS_ENABLED, false);
    }
    public long getNextAlarm(Context c) {
        if(prefs == null) {
            prefs = new TommanPrefs(c);
        }
        return prefs.getPreferences().getLong(NEXT_ALARM, -1);
    }

    public void enableReminder(Context c, boolean enabled) {
        if(prefs == null) {
            prefs = new TommanPrefs(c);
        }
        if (isReminderEnabled(c) == enabled) return;
        prefs.getPreferences().edit().putBoolean(REMINDERS_ENABLED, enabled).apply();
        invalidate(c);
    }

    public void setQuietTime(Context c, Date start, Date end) {
        if(prefs == null) {
            prefs = new TommanPrefs(c);
        }
        long current_alarm_long = getNextAlarm(c);
        Date current_alarm = new Date(current_alarm_long);
        prefs.getPreferences()
                .edit()
                .putLong(QUIET_TIME_START, start.getTime())
                .putLong(QUIET_TIME_END, end.getTime())
                .apply();


        if (isBetweenStartAndEnd(current_alarm, start, end)) {
            invalidate(c);
        }
    }

    private boolean isBetweenStartAndEnd(Context c, Calendar time) {
        return isBetweenStartAndEnd(time.getTime(), new Date(getQuietTimeStart(c)), new Date(getQuietTimeEnd(c)));
    }

    private boolean isBetweenStartAndEnd(Date target, Date start, Date end) {
        Calendar tarCal = Calendar.getInstance();
        Calendar startCal = Calendar.getInstance();
        Calendar endCal = Calendar.getInstance();
        tarCal.setTime(target);
        startCal.setTime(start);
        endCal.setTime(end);
        return isBetweenStartAndEnd(tarCal, startCal, endCal);
    }

    private boolean isBetweenStartAndEnd(Calendar target, Calendar start, Calendar end) {
        start.set(Calendar.YEAR, target.get(Calendar.YEAR));
        start.set(Calendar.DAY_OF_YEAR, target.get(Calendar.DAY_OF_YEAR));
        end.set(Calendar.YEAR, target.get(Calendar.YEAR));
        end.set(Calendar.DAY_OF_YEAR, target.get(Calendar.DAY_OF_YEAR));

        if (start.before(end)) {
            return (target.before(end) && start.before(target));
        }else{
            return end.before(start) && (target.before(end) || start.before(target));
        }
    }
}
