package org.gbe.tomman.client.misc;

import android.os.Parcel;
import android.os.Parcelable;

import org.gbe.tomman.client.retrofit.DoctorDTO;

/**
 * Created by gbe on 11/19/14.
 */
public class Doctor implements Parcelable {
    private long id;
    private String userName;
    private String firstName;
    private String lastName;

    public Doctor(Parcel p) {
        id = p.readLong();
        userName = p.readString();
        firstName = p.readString();
        lastName = p.readString();
    }

    public Doctor(DoctorDTO dto) {
        id = dto.getId();
        userName = dto.getUserName();
        firstName = dto.getFirstName();
        lastName = dto.getLastName();
    }

    public DoctorDTO getDto() {
        DoctorDTO dto = new DoctorDTO();
        dto.setId(this.id);
        dto.setUserName(this.userName);
        dto.setFirstName(this.firstName);
        dto.setLastName(this.lastName);
        return dto;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(userName);
        parcel.writeString(firstName);
        parcel.writeString(lastName);
    }

    public static final Parcelable.Creator<Doctor> CREATOR = new Creator<Doctor>() {
        @Override
        public Doctor createFromParcel(Parcel parcel) {
            return new Doctor(parcel);
        }

        @Override
        public Doctor[] newArray(int i) {
            return new Doctor[i];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
}
