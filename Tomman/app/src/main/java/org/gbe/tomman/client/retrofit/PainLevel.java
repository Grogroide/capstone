package org.gbe.tomman.client.retrofit;

public enum PainLevel {
	WELL_CONTROLLED, 
	MODERATE, 
	SEVERE
}

