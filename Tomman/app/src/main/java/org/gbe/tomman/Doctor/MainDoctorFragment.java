package org.gbe.tomman.Doctor;

import android.app.Activity;
import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.gbe.tomman.LoginActivity;
import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Doctor;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.TommanPrefs;
import org.gbe.tomman.client.retrofit.DoctorDTO;
import org.gbe.tomman.client.retrofit.PatientDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;

import java.util.Calendar;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;


public class MainDoctorFragment extends Fragment {

    protected List<Patient> patients;
    public static final String SENDER_ID = "240005357739";


    private MainDoctorActionsListener mActivityCallback;

    public interface MainDoctorActionsListener {
        void onPatientsButtonClicked();
        void onAlertsButtonClicked();
        void onMedicationsButtonClicked();
        void onSearchButtonClicked(String searchString);
    }

    @InjectView(R.id.tv_doc_user)
    TextView tvDocUser;

    @InjectView(R.id.et_search)
    EditText etSearch;

    @InjectView(R.id.btn_search)
    Button btnSearch;

    @InjectView(R.id.btn_alerts)
    Button btnAlerts;

    private Doctor doctor;

    private TommanSvcApi tomman;

    private TommanPrefs prefs;

    private String gcmId;


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_main_doctor, container, false);
        ButterKnife.inject(this, view);

        return view;
    }

    @Override
    public void onResume(){
        super.onResume();
        prefs = new TommanPrefs(getActivity().getApplicationContext());
        tomman = TommanSvc.getOrShowLogin(getActivity());
        if (tomman != null) {
            String s = prefs.getPreferences().getString(TommanPrefs.LOGGED_AS, null);
            if ((TommanPrefs.DOCTOR.equals(s))) {
                fetchDoctor();
                fetchAlerts();
                registerGCM();
            } else {
                logout();
            }
        }
    }

    @Override
    public void onAttach(Activity hostActivity) {
        super.onAttach(hostActivity);
        try {
            mActivityCallback = (MainDoctorActionsListener) hostActivity;
        }
        catch (ClassCastException e) {
            throw new ClassCastException(hostActivity.toString()
                    + " must implement MainDoctorActionsListener");
        }
    }

    private void registerGCM() {
        CallableTask.invoke(
                new Callable<String>(){
                    @Override
                    public String call() throws Exception {
                        GoogleCloudMessaging gcm = GoogleCloudMessaging.getInstance(getActivity().getApplicationContext());
                        return gcm.register(SENDER_ID);
                    }
                },
                new TaskCallback<String>() {
                    @Override
                    public void success(String result) {
                        gcmId = result;
                        postGCMId();
                    }

                    @Override
                    public void error(Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), "GCM registration failed", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
        );
    }

    private void postGCMId() {
        CallableTask.invoke(
                new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        return tomman.registerGCMId(gcmId);
                    }
                },
                new TaskCallback<Boolean>() {
                    @Override
                    public void success(Boolean result) {
                        Toast.makeText(getActivity().getApplicationContext(), "GCM registration successful", Toast.LENGTH_SHORT).show();
                    }

                    @Override
                    public void error(Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), "GCM registration failed", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
        );
    }

    private void fetchDoctor() {
        CallableTask.invoke(
                new Callable<DoctorDTO>() {
                    @Override
                    public DoctorDTO call() throws Exception {
                        return tomman.getDoctor();
                    }
                },
                new TaskCallback<DoctorDTO>() {
                    @Override
                    public void success(DoctorDTO result) {
                        if(result != null)
                            doctor = new Doctor(result);
                            tvDocUser.setText(doctor.getFirstName() + " " + doctor.getLastName());
                    }

                    @Override
                    public void error(Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), "Error while retrieving doctor info", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                });
    }

    private void fetchAlerts() {
        CallableTask.invoke(
                new Callable<Collection<PatientDTO>>(){
                    @Override
                    public Collection<PatientDTO> call() throws Exception {
                        Calendar c = Calendar.getInstance();
                        c.add(Calendar.DATE, -2);
                        return tomman.getAlerts(c.getTimeInMillis());
                    }
                },
                new TaskCallback<Collection<PatientDTO>>() {
                    @Override
                    public void success(Collection<PatientDTO> result) {
                        btnAlerts.setText(getResources().getString(R.string.btn_patient_alerts_string) + " (" + result.size() + ")");
                    }

                    @Override
                    public void error(Exception e) {
                        Toast.makeText(getActivity().getApplicationContext(), "Error while retrieving alerts info", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
        );
    }

    private void launchLoginActivity() {
        Intent i = new Intent(getActivity().getBaseContext(), LoginActivity.class);
        startActivity(i);
    }

    @OnClick(R.id.btn_medications)
    public void launchMedicationsActivity(){
        mActivityCallback.onMedicationsButtonClicked();
    }

    @OnClick(R.id.btn_alerts)
    public void launchAlertsActivity(){
        mActivityCallback.onAlertsButtonClicked();
    }


    @OnClick(R.id.btn_goto_checkin)
    public void launchPatientsListActivity(){
        mActivityCallback.onPatientsButtonClicked();
    }

    @OnClick(R.id.btn_search)
    public void search() {
        if (etSearch.getText().length() > 0) {
            mActivityCallback.onSearchButtonClicked(etSearch.getText().toString());
        }
    }

    private void logout() {
        prefs.getPreferences().edit().putString(TommanPrefs.LOGGED_AS, null).apply();
        prefs.getPreferences().edit().putString(TommanPrefs.LOGGING_AS, null).apply();
        TommanSvc.logout();
        startActivity(new Intent(getActivity(), LoginActivity.class));
    }
}
