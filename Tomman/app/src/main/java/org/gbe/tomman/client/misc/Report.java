package org.gbe.tomman.client.misc;

import android.support.annotation.NonNull;

import org.gbe.tomman.client.retrofit.CheckInDTO;

import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;

import org.gbe.tomman.client.retrofit.TookMedicationDTO;

/**
 * Created by gbe on 11/16/14.
 */
public class Report implements Comparable<Report>{
    public final static int DAYBREAK = 0;
    public final static int CHECKIN = 1;
    public final static int TOOKMEDICATION = 2;

    protected TookMedicationDTO _tm;
    protected CheckInDTO ci;
    protected long timestamp;
    protected String dayString;
    protected String timeString;
    //final private boolean _isTookmedication;
    //final private boolean _isCheckIn;

    final private int _type;



    public Report(TookMedicationDTO tm){
        _type = TOOKMEDICATION;
        _tm = tm;
        timestamp = tm.getTimestamp();
        ci = null;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        timeString = sdf.format(new Date(timestamp));
        sdf.applyPattern("EEEE, MMMM d");
        dayString = sdf.format(new Date(timestamp));
    }

    public Report(CheckInDTO ci) {
        _type = CHECKIN;
        _tm = null;
        timestamp = ci.getTimestamp();
        this.ci = ci;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        timeString = sdf.format(new Date(timestamp));
        sdf.applyPattern("EEEE, MMMM d");
        dayString = sdf.format(new Date(timestamp));
    }

    public Report(long timestampOfDayBreak){
        _type = DAYBREAK;
        _tm = null;
        ci = null;
        timestamp = timestampOfDayBreak;
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        timeString = sdf.format(new Date(timestamp));
        sdf.applyPattern("EEEE, MMMM d");
        dayString = sdf.format(new Date(timestamp));
    }


    public int getType(){
        return _type;
    }

//    public boolean isTookMedication(){return _isTookmedication;}
//
//    public boolean isCheckIn(){return _isCheckIn;}

    public TookMedicationDTO get_tm() {
        return _tm;
    }

    public void set_tm(TookMedicationDTO _tm) {
        this._tm = _tm;
    }

    public CheckInDTO getCi() {
        return ci;
    }

    public void setCi(CheckInDTO ci) {
        this.ci = ci;
    }

    public long getTimeStamp() {
        return timestamp;
    }

    public String getDayString() {
        return dayString;
    }

    public String getTimeString() {
        return timeString;
    }
    @Override
    public int compareTo(@NonNull Report report) {
        return (getTimeStamp() > report.getTimeStamp() ) ? 1 : -1;
    }

    public static final class ReverseComparator implements Comparator<Report> {
        @Override
        public int compare(Report report, Report t1) {
            if (report == null || t1 == null){
                throw new NullPointerException();
            }
            return t1.compareTo(report);
        }
    }
}
