package org.gbe.tomman.client.retrofit;

public enum TommanApiRole {
	Admin,
	Doctor,
	Patient
}
