package org.gbe.tomman.Doctor;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.PatientAdapter;
import org.gbe.tomman.client.retrofit.PatientDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnItemClick;


public class SearchPatientListFragment extends Fragment {
    public static final String SEARCH_STRING_KEY = "Search_String_Key";

    private static final String TAG = "SearchPatientListFrag";

    @InjectView(R.id.lv_patientList_search)
    protected ListView lvPatientListSearch;

    private List<Patient> patients;

    private PatientAdapter patientAdapter;

    private TommanSvcApi tomman;

    private String searchString;

    private OnSearchListPatientClickedListener mItemClickedCallback;

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_search_patient_list, container, false);
        ButterKnife.inject(this, view);
        patients =  new ArrayList<Patient>();
        patientAdapter = new PatientAdapter(getActivity(), R.layout.patient_row, patients);
        lvPatientListSearch.setAdapter(patientAdapter);
        getSearchStringFromArgs();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        tomman = TommanSvc.getOrShowLogin(getActivity());
        fetchPatients();
    }

    private void fetchPatients() {
        if((searchString != null) && (searchString.length() > 0)){
            CallableTask.invoke(
                new GetPatientsCallable(),
                new OnPatientReceivedCallback()
            );
        }

    }

    @Override
    public void onAttach(Activity hostActivity) {
        super.onAttach(hostActivity);
        try {
            mItemClickedCallback = (OnSearchListPatientClickedListener)hostActivity;
        } catch (ClassCastException e) {
            throw new ClassCastException(hostActivity.toString() + "must implement " +
                    "OnSearchListPatientClickedListener");
        }
    }

    @Override
    public void onStart() {
        super.onStart();
        if (getFragmentManager().findFragmentById(R.id.leftPanel) != null) {
            lvPatientListSearch.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
        }
    }

    private void getSearchStringFromArgs() {
        Bundle b = getArguments();
        searchString = b.getString(SEARCH_STRING_KEY);
    }

    @OnItemClick(R.id.lv_patientList_search)
    void onItemClick(int position){
        Patient p = patients.get(position);
        mItemClickedCallback.OnSearchListPatientClicked(p);
    }

    private class GetPatientsCallable implements Callable<Collection<PatientDTO>> {
        @Override
        public Collection<PatientDTO> call() throws Exception {
              return tomman.searchPatientByName(searchString);
        }
    }

    private class OnPatientReceivedCallback implements TaskCallback<Collection<PatientDTO>> {
        @Override
        public void success(Collection<PatientDTO> result) {
            patients.clear();
            for(PatientDTO dto : result){
                patients.add(new Patient(dto));
            }
            patientAdapter.notifyDataSetChanged();
        }
        @Override
        public void error(Exception e) {
            Log.e(TAG, "Error while retrieving Patients : " + e.getMessage());
            e.printStackTrace();
        }
    }


    public interface OnSearchListPatientClickedListener {
        void OnSearchListPatientClicked(Patient p);
    }
}
