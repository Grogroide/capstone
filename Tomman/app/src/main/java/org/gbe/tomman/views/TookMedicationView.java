package org.gbe.tomman.views;

import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import org.gbe.tomman.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.gbe.tomman.client.retrofit.MedicationDTO;
import org.gbe.tomman.client.retrofit.TookMedicationDTO;

/**
 * Created by gbe on 11/16/14.
 */
public class TookMedicationView extends LinearLayout {

    private TookMedicationDTO tookMed;
    private MedicationDTO medication;

    private Calendar tmCalendar;

    private TextView mQuestion;
    private TextView mDate;
    private Switch mSwitch;

    public TookMedicationView(Context context, AttributeSet attrs, MedicationDTO medicationDTO){
        super(context, attrs);
        this.medication = medicationDTO;
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.tookmedication_row, this);
        loadViews();
    }

    public TookMedicationView(Context context, MedicationDTO medicationDTO) {
        super(context);
        this.medication = medicationDTO;
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.tookmedication_row, this);
        loadViews();
    }

    private void loadViews() {
        mQuestion = (TextView)findViewById(R.id.tv_tookmed_prompt_question);
        mDate = (TextView)findViewById(R.id.tv_tookmedication_date);
        mSwitch = (Switch)findViewById(R.id.sw_tookmed);

        //set default values
        mQuestion.setText(getContext().getString(R.string.tookmed_prompt_question) + " " + medication.getName() + "?");
        mSwitch.setChecked(false);
        mDate.setText("");
        //set listener
        mSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    promptForDate();

                }else {
                    tookMed = null;
                    tmCalendar = null;
                    mDate.setText("");
                }
            }
        });
    }

    private void promptForDate() {
        tmCalendar = Calendar.getInstance();
        DatePickerDialog dlg = new DatePickerDialog(
                this.getContext(),
                new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker datePicker, int i, int i2, int i3) {
                        if(datePicker.isShown()) {
                            tmCalendar.set(Calendar.YEAR, i);
                            tmCalendar.set(Calendar.MONTH, i2);
                            tmCalendar.set(Calendar.DAY_OF_MONTH, i3);
                            promptForTime();
                        }
                    }
                },
                Calendar.getInstance().get(Calendar.YEAR),
                Calendar.getInstance().get(Calendar.MONTH),
                Calendar.getInstance().get(Calendar.DAY_OF_MONTH)
        );
        dlg.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (tookMed == null) {
                    mSwitch.setChecked(false);
                }
            }
        });
        dlg.show();
    }

    private void promptForTime() {
        TimePickerDialog dlg = new TimePickerDialog(
                this.getContext(),
                new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i2) {
                        if(timePicker.isShown()){
                            tmCalendar.set(Calendar.HOUR_OF_DAY, i);
                            tmCalendar.set(Calendar.MINUTE, i2);
                            tookMed = new TookMedicationDTO(tmCalendar.getTimeInMillis(), medication.getId(), 0, 1 );
                            SimpleDateFormat sdf = new SimpleDateFormat("EEEE, K:mm a");
                            mDate.setText(sdf.format(tmCalendar.getTime()));
                        }
                    }
                },
                Calendar.getInstance().get(Calendar.HOUR_OF_DAY),
                Calendar.getInstance().get(Calendar.MINUTE),
                true);
        dlg.setOnCancelListener(new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialogInterface) {
                if (tookMed == null) {
                    mSwitch.setChecked(false);
                }
            }
        });
        dlg.show();
    }


    public void setMedication(MedicationDTO dto) {
        this.medication = dto;
    }

    public TookMedicationDTO getTookMedication() {
        return tookMed;
    }

    public TookMedicationDTO getTookMedicationDto() {
        return tookMed; // can be null
    }
}
