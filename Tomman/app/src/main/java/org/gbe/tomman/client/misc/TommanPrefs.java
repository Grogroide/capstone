package org.gbe.tomman.client.misc;

import android.content.Context;
import android.content.SharedPreferences;

import java.io.StringBufferInputStream;

/**
 * Created by gbe on 11/22/14.
 */
public class TommanPrefs {
    public static String LOGGED_AS = "logged_as";
    public static String LOGGING_AS = "logging_as";

    public final static String DOCTOR = "doctor";
    public final static String PATIENT = "patient";

    final private static String prefsFileName = "TommanPreferences";

    final private SharedPreferences prefs;

    public TommanPrefs(Context context){
        prefs = context.getSharedPreferences(prefsFileName, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPreferences() {
        return prefs;
    }
}
