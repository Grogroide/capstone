package org.gbe.tomman.Doctor;

import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import org.gbe.tomman.LoginActivity;
import org.gbe.tomman.R;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.TommanPrefs;
import org.gbe.tomman.client.retrofit.TommanSvcApi;


public class DoctorMainFragmentedActivity extends FragmentActivity
        implements
        MainDoctorFragment.MainDoctorActionsListener,
        AlertsListFragment.OnPatientSelectedListener,
        PatientsListFragment.OnPatientSelectedListener,
        SearchPatientListFragment.OnSearchListPatientClickedListener,
        PatientReportFragment.OnPrescriptionButtonClickedListener
{
    private static final String TAG_ONLY_PANEL = "only_panel";
    private static final String TAG_LEFT_PANEL = "left_pane";

    private TommanSvcApi tomman;

    private TommanPrefs prefs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.doctor_fragmented_activity);
        prefs = new TommanPrefs(getApplicationContext());
        if (findViewById(R.id.onlyPanel) != null) { // small screen
            if (getFragmentManager().findFragmentByTag(TAG_ONLY_PANEL)  == null ) {
                if (savedInstanceState != null) {
                    return;
                }
                MainDoctorFragment fragment = new MainDoctorFragment();
                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.onlyPanel, fragment)
                        .commit();
            }
        } else {
            if (findViewById(R.id.leftPanel) != null) {
                if(getFragmentManager().findFragmentByTag(TAG_LEFT_PANEL) == null) {
                    if (savedInstanceState != null) {
                        return;
                    }
                    MainDoctorFragment fragment = new MainDoctorFragment();
                    getFragmentManager()
                            .beginTransaction()
                            .add(R.id.leftPanel, fragment)
                            .commit();
                }
            }
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main_doctor_fragmented, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();

        int id = item.getItemId();
        if (id == R.id.action_logout) {
            logout();
            return true;
        }
        if (id == R.id.log_backstack) {
            displayBackStack(getFragmentManager());
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onAlertsButtonClicked() {
        AlertsListFragment fragment = new AlertsListFragment();
        attachFragmentToMainPanel(fragment);
    }

    @Override
    public void onMedicationsButtonClicked() {
        MedicationListFragment fragment = new MedicationListFragment();
        attachFragmentToMainPanel(fragment);
    }

    @Override
    public void onPatientsButtonClicked() {
        PatientsListFragment fragment = new PatientsListFragment();
        attachFragmentToMainPanel(fragment);
    }

    @Override
    public void onPatientSelected(Patient p) {
        PatientReportFragment fragment = new PatientReportFragment();
        Bundle b = new Bundle();
        b.putParcelable(PatientReportFragment.PATIENT_KEY, p);
        fragment.setArguments(b);
        attachFragmentToSecondaryPanel(fragment);
    }

    private void attachFragmentToMainPanel(Fragment fragment) {
        if (findViewById(R.id.onlyPanel) != null){ // 1-pane layout
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.onlyPanel, fragment)
                    .addToBackStack(fragment.toString())
                    .commit();
        } else { // 2-pane layout, we replace the leftPanel's content
            if (findViewById(R.id.leftPanel) != null) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.leftPanel, fragment)
                        .addToBackStack(fragment.toString())
                        .commit();
            } else {
                throw new NullPointerException("No panel in current view");
            }
        }
    }

    /**
     * If in two-panel mode, place the fragment in the right panel. If only one panel, will replace
     * the only panel.
     * @param fragment The Fragment to attach to the secondary panel
     */
    private void attachFragmentToSecondaryPanel(Fragment fragment) {
        if (findViewById(R.id.onlyPanel) != null){ // 1-pane layout
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.onlyPanel, fragment)
                    .addToBackStack(fragment.toString())
                    .commit();
        } else { // 2-pane layout, we replace the leftPanel's content
            if (findViewById(R.id.rightPanel) != null) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.rightPanel, fragment)
                        .addToBackStack(fragment.toString())
                        .commit();
            } else {
                throw new NullPointerException("No panel in current view");
            }
        }
    }

    public static void displayBackStack(FragmentManager fm) {
        int count = fm.getBackStackEntryCount();
        Log.d("Backstack log", "There are " + count + " entries");
        for(int i = 0; i<count; i++) {
            // Display Backstack-entry data like
            String name = fm.getBackStackEntryAt(i).getName();
            Log.d("Backstack log", "entry " + i + ": " + name);
        }
    }

    private void logout() {
        prefs.getPreferences().edit()
                .putString(TommanPrefs.LOGGED_AS, null)
                .putString(TommanPrefs.LOGGING_AS, null)
                .commit();
        TommanSvc.logout();
        startActivity(new Intent(DoctorMainFragmentedActivity.this, LoginActivity.class));
    }

    @Override
    public void OnSearchListPatientClicked(Patient p) {
        PatientReportFragment fragment = new PatientReportFragment();
        Bundle b = new Bundle();
        b.putParcelable(PatientReportFragment.PATIENT_KEY, p);
        fragment.setArguments(b);
        attachFragmentToSecondaryPanel(fragment);
    }

    @Override
    public void onSearchButtonClicked(String searchString) {
        SearchPatientListFragment fragment = new SearchPatientListFragment();
        Bundle b = new Bundle();
        b.putString(SearchPatientListFragment.SEARCH_STRING_KEY, searchString);
        fragment.setArguments(b);
        attachFragmentToMainPanel(fragment);
    }

    @Override
    public void onPrescriptionButtonClicked(Patient p) {
        PrescriptionFragment fragment = new PrescriptionFragment();
        Bundle b = new Bundle();
        b.putParcelable(PatientReportFragment.PATIENT_KEY, p);
        fragment.setArguments(b);
        attachFragmentToSecondaryPanel(fragment);
    }
}
