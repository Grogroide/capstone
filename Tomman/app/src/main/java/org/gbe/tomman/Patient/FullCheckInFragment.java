package org.gbe.tomman.Patient;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.Callable;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;

import org.gbe.tomman.R;
import org.gbe.tomman.client.CallableTask;
import org.gbe.tomman.client.TaskCallback;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Medication;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.Reminder;
import org.gbe.tomman.client.retrofit.CheckInDTO;
import org.gbe.tomman.client.retrofit.EatingDifficulty;
import org.gbe.tomman.client.retrofit.PainLevel;
import org.gbe.tomman.client.retrofit.PatientDTO;
import org.gbe.tomman.client.retrofit.TommanSvcApi;
import org.gbe.tomman.client.retrofit.TookMedicationDTO;
import org.gbe.tomman.views.TookMedicationView;


public class FullCheckInFragment extends Fragment {

    @InjectView(R.id.questionlistlayout)
    LinearLayout questionList;

    @InjectView(R.id.rb_eat_difficulty0)
    RadioButton rbEatDifficulty0;
    @InjectView(R.id.rb_eat_difficulty1)
    RadioButton rbEatDifficulty1;
    @InjectView(R.id.rb_eat_difficulty2)
    RadioButton rbEatDifficulty2;

    @InjectView(R.id.rb_pain0)
    RadioButton rbPain0;
    @InjectView(R.id.rb_pain1)
    RadioButton rbPain1;
    @InjectView(R.id.rb_pain2)
    RadioButton rbPain2;

    @InjectView(R.id.rgrp_pain_prompt)
    RadioGroup rgrpPainPrompt;
    @InjectView(R.id.rgrp_eat_difficulty_prompt)
    RadioGroup rgrpEatDifficultyPrompt;

    protected ArrayList<TookMedicationView> medicationPromptViews;

    @InjectView(R.id.btn_sendcheckin)
    Button btn_sendcheckin;

    private CheckInDTO checkInWithId;
    private CheckInDTO checkInToSend;

    private Patient patient;

    private TommanSvcApi tomman;

    private OnCheckInSuccessfulListener mCheckInSuccessfulCallback;

    @Override
    public View onCreateView(
            LayoutInflater inflater,
            ViewGroup container,
            Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        View view = inflater.inflate(R.layout.fragment_full_check_in, container, false);
        ButterKnife.inject(this, view);
        tomman = TommanSvc.getOrShowLogin(getActivity());
        retrievePatientInfo();
        return view;
    }

    private void retrievePatientInfo() {
        CallableTask.invoke(
                new Callable<PatientDTO>() {
                    @Override
                    public PatientDTO call() throws Exception {
                        return tomman.getPatientInfo();
                    }
                },
                new TaskCallback<PatientDTO>() {
                    @Override
                    public void success(PatientDTO result) {
                        patient = new Patient(result);
                        injectMedicationViews();
                    }

                    @Override
                    public void error(Exception e) {
                        Toast.makeText(getActivity(), "Error retrieving patient info", Toast.LENGTH_SHORT).show();
                        e.printStackTrace();
                    }
                }
        );
    }

    private void injectMedicationViews() {
        medicationPromptViews = new ArrayList<TookMedicationView>();
        ArrayList<Medication> prescriptions = (ArrayList<Medication>) patient.getPrescriptions();
        for(Medication med : prescriptions){
            TookMedicationView tmv = new TookMedicationView(questionList.getContext(), med.getMedicationDto());
            medicationPromptViews.add(tmv);
            questionList.addView(tmv);
        }
    }

    @OnClick(R.id.btn_sendcheckin)
    protected void sendCheckin() {
        //validate
        boolean cancel = false;

        int eatDifficultyValueId = rgrpEatDifficultyPrompt.getCheckedRadioButtonId();
        int painValueId = rgrpPainPrompt.getCheckedRadioButtonId();
        ArrayList<TookMedicationDTO> tookMedicationList = new ArrayList<TookMedicationDTO>();

        for(TookMedicationView view : this.medicationPromptViews) {
            TookMedicationDTO dto = view.getTookMedicationDto();
            if(dto != null) {
                dto.setPatientId(patient.getId());
                tookMedicationList.add(dto);
            }
        }
        if ( eatDifficultyValueId == -1) {
            cancel = true;
        }
        if ( painValueId == -1) {
            cancel = true;
        }
        if (cancel) {
            Toast.makeText(getActivity(), "Please review your Check-In", Toast.LENGTH_LONG).show();
            return;
        }
        //build CheckinDTO
        checkInToSend = new CheckInDTO();

        switch (eatDifficultyValueId) {
            case R.id.rb_eat_difficulty0:
                checkInToSend.setEatingDifficulty(EatingDifficulty.NO);
                break;
            case R.id.rb_eat_difficulty1:
                checkInToSend.setEatingDifficulty(EatingDifficulty.SOME);
                break;
            case R.id.rb_eat_difficulty2:
                checkInToSend.setEatingDifficulty(EatingDifficulty.I_CANT_EAT);
                break;
            default:
                Toast.makeText(getActivity(), "Unknown value id", Toast.LENGTH_LONG).show();
                return;
        }
        switch(painValueId) {
            case R.id.rb_pain0:
                checkInToSend.setPainLevel(PainLevel.WELL_CONTROLLED);
                break;
            case R.id.rb_pain1 :
                checkInToSend.setPainLevel(PainLevel.MODERATE);
                break;
            case R.id.rb_pain2:
                checkInToSend.setPainLevel(PainLevel.SEVERE);
                break;
            default:
                Toast.makeText(getActivity(), "Unknown value id", Toast.LENGTH_LONG).show();
                return;
        }
        Date date = Calendar.getInstance().getTime();
        checkInToSend.setTimestamp(date.getTime());
        checkInToSend.setTookMedications(tookMedicationList);

        Toast.makeText(getActivity(), date.toString(), Toast.LENGTH_SHORT).show();

        //start aSyncTask for sending.
        CallableTask.invoke(
                new Callable<CheckInDTO>() {

                    @Override
                    public CheckInDTO call() throws Exception {
                        return tomman.checkIn(checkInToSend);
                    }
                },
                new TaskCallback<CheckInDTO>() {
                    @Override
                    public void success(CheckInDTO result) {
                        Toast.makeText(getActivity(), "Check-in OK", Toast.LENGTH_SHORT).show();
                        checkInWithId = result;
                        Reminder r = new Reminder();
                        r.init(getActivity().getApplicationContext());
                        r.invalidate(getActivity().getApplicationContext());
                        mCheckInSuccessfulCallback.onCheckInSuccessful(FullCheckInFragment.this);
                    }

                    @Override
                    public void error(Exception e) {
                        Toast.makeText(getActivity(), "Error: Check-In could not be sent", Toast.LENGTH_LONG).show();
                    }
                }
        );
    }

    public interface OnCheckInSuccessfulListener {
        void onCheckInSuccessful(Fragment f);
    }

    @Override
    public void onAttach(Activity hostActivity) {
        super.onAttach(hostActivity);
        try {
            mCheckInSuccessfulCallback = (OnCheckInSuccessfulListener)hostActivity;
        } catch (ClassCastException e) {
            throw new ClassCastException(hostActivity.toString() +
                    " must implement OnCheckInSuccessfulListener");
        }
    }
}
