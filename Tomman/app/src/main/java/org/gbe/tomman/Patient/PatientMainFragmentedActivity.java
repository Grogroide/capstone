package org.gbe.tomman.Patient;

import android.app.Fragment;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;


import org.gbe.tomman.LoginActivity;
import org.gbe.tomman.Patient.FullCheckInFragment;
import org.gbe.tomman.Patient.HistoryFragment;
import org.gbe.tomman.Patient.PatientMainFragment;
import org.gbe.tomman.Patient.PatientSettingsFragment;
import org.gbe.tomman.Patient.PrescriptionsFragment;
import org.gbe.tomman.R;
import org.gbe.tomman.client.TommanSvc;
import org.gbe.tomman.client.misc.Medication;
import org.gbe.tomman.client.misc.Patient;
import org.gbe.tomman.client.misc.TommanPrefs;

import java.util.ArrayList;


public class PatientMainFragmentedActivity extends FragmentActivity
                    implements
        PatientMainFragment.OnPatientMainScreenButtonClickedListener,
        FullCheckInFragment.OnCheckInSuccessfulListener
{

    private static final String TAG_ONLY_PANEL = "only_panel";
    private static final String TAG_LEFT_PANEL = "left_pane";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_fragmented_activity);

        if (findViewById(R.id.onlyPanel) != null) {
            if (getFragmentManager().findFragmentByTag(TAG_ONLY_PANEL) == null) {
                Fragment main = new PatientMainFragment();
                getFragmentManager()
                        .beginTransaction()
                        .add(R.id.onlyPanel, main, TAG_ONLY_PANEL)
                        .commit();
            }
        } else {
            if (getFragmentManager().findFragmentByTag(TAG_LEFT_PANEL) == null) {
                if (findViewById(R.id.leftPanel) != null) {
                    Fragment main = new PatientMainFragment();
                    getFragmentManager()
                            .beginTransaction()
                            .add(R.id.leftPanel, main, TAG_LEFT_PANEL)
                            .commit();
                }
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.patient_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            logout();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    private void logout() {
        TommanPrefs prefs = new TommanPrefs(getApplicationContext());
        prefs.getPreferences().edit()
                .putString(TommanPrefs.LOGGED_AS, null)
                .putString(TommanPrefs.LOGGING_AS, null)
                .commit();
        TommanSvc.logout();
        startActivity(new Intent(PatientMainFragmentedActivity.this, LoginActivity.class));
    }

    @Override
    public void onCheckInSuccessful(Fragment fragment) {
        if (findViewById(R.id.onlyPanel) != null) {
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.onlyPanel, new PatientMainFragment())
                    .commit();
        }
        removeFragment(fragment);
    }

    @Override
    public void onCheckInButtonClicked() {
        Fragment f = new FullCheckInFragment();
        attachFragmentToSecondaryPanel(f);
    }

    @Override
    public void onHistoryButtonClicked(Patient p) {
        HistoryFragment f = HistoryFragment.newInstance(p);
        attachFragmentToSecondaryPanel(f);
    }

    @Override
    public void onPrescriptionsButtonClicked(ArrayList<Medication> prescriptions) {
        PrescriptionsFragment f = PrescriptionsFragment.newInstance(prescriptions);
        attachFragmentToSecondaryPanel(f);
    }

    @Override
    public void onPatientSettingsButtonClicked() {
        Fragment f = new PatientSettingsFragment();
        attachFragmentToSecondaryPanel(f);
    }
    /**
     * If in two-panel mode, place the fragment in the left panel. If only one panel, will replace
     * the only panel.
     * @param fragment The Fragment to attach to the secondary panel
     */
    private void attachFragmentToMainPanel(Fragment fragment) {
        if (findViewById(R.id.onlyPanel) != null){ // 1-pane layout
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.onlyPanel, fragment)
                    .addToBackStack(fragment.toString())
                    .commit();
        } else { // 2-pane layout, we replace the leftPanel's content
            if (findViewById(R.id.leftPanel) != null) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.leftPanel, fragment)
                        .addToBackStack(fragment.toString())
                        .commit();
            } else {
                throw new NullPointerException("No panel in current view");
            }
        }
    }

    /**
     * If in two-panel mode, place the fragment in the right panel. If only one panel, will replace
     * the only panel.
     * @param fragment The Fragment to attach to the secondary panel
     */
    private void attachFragmentToSecondaryPanel(Fragment fragment) {
        if (findViewById(R.id.onlyPanel) != null){ // 1-pane layout
            getFragmentManager()
                    .beginTransaction()
                    .replace(R.id.onlyPanel, fragment)
                    .addToBackStack(fragment.toString())
                    .commit();
        } else { // 2-pane layout, we replace the leftPanel's content
            if (findViewById(R.id.rightPanel) != null) {
                getFragmentManager()
                        .beginTransaction()
                        .replace(R.id.rightPanel, fragment)
                        .addToBackStack(fragment.toString())
                        .commit();
            } else {
                throw new NullPointerException("No panel in current view");
            }
        }
    }

    private void removeFragment(Fragment fragment) {
        if (findViewById(R.id.onlyPanel) != null) {
            getFragmentManager().beginTransaction().remove(fragment).commit();
        }else{
            if (findViewById(R.id.leftPanel) != null) {
                getFragmentManager().beginTransaction().remove(fragment).commit();
            }
        }
    }

}
