package org.gbe.tomman.client;

import android.content.Context;
import android.content.Intent;

import org.gbe.tomman.LoginActivity;

import org.gbe.tomman.client.oauth.SecuredRestBuilder;
import org.gbe.tomman.client.retrofit.TommanSvcApi;
import org.gbe.tomman.client.unsafe.EasyHttpClient;
import retrofit.RestAdapter;
import retrofit.client.ApacheClient;

/**
 * Created by gbe on 11/9/14.
 *
 */



public class TommanSvc {

    public static final String CLIENT_ID ="mobile";

    private static TommanSvcApi tommanSvc;

    public static synchronized TommanSvcApi getOrShowLogin(Context ctx) {
        if (tommanSvc != null) {
            return tommanSvc;
        }else {
            Intent i = new Intent(ctx, LoginActivity.class);
            ctx.startActivity(i);
            return null;
        }
    }

    public static synchronized TommanSvcApi init(String server, String user, String pass) {
        tommanSvc = new SecuredRestBuilder()
                .setLoginEndpoint(server + TommanSvcApi.TOKEN_PATH)
                .setUsername(user)
                .setPassword(pass)
                .setClientId(CLIENT_ID)
                .setClient(new ApacheClient(new EasyHttpClient()))
                .setEndpoint(server).setLogLevel(RestAdapter.LogLevel.FULL)
                .build()
                .create(TommanSvcApi.class);
        return tommanSvc;
    }

    public static synchronized void logout(){
        tommanSvc = null;
    }
}
