package org.gbe.tomman.client.retrofit;

public enum EatingDifficulty {
	NO,
	SOME,
	I_CANT_EAT
}
